# MarketliaPlus

Proyecto de la materia de aplicaciones web, esta aplicación tiene como objetivo ser una una plataforma de compra/venta de inmobiliaria en el país.

# <center>Mockups</center>

Los Mockups realizados usando la herramienta Figma se encuentran en el siguiente enlace:

https://www.figma.com/file/br7p3IwwgX5FfBx1qU62Na/Untitled?node-id=0%3A1

# <center>Historias de Usuario</center>

Las historias de usuario se encuentra en este mismo proyecto de GitLab, en la sección de Issues,
en el board de Historia de Usuario



