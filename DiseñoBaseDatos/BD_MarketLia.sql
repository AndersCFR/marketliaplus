-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Usuario` (
  `idUsuario` INT NOT NULL,
  `nombreUser` VARCHAR(45) NULL,
  `apellidoUser` VARCHAR(45) NULL,
  `documentoUser` VARCHAR(45) NULL,
  PRIMARY KEY (`idUsuario`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`table2`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`table2` (
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Provincia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Provincia` (
  `idProvincia` INT NOT NULL,
  `nombreProvincia` VARCHAR(45) NULL,
  PRIMARY KEY (`idProvincia`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Ciudad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Ciudad` (
  `idCiudad` INT NOT NULL,
  `nombreCiudad` VARCHAR(45) NULL,
  `Provincia_idProvincia` INT NOT NULL,
  PRIMARY KEY (`idCiudad`),
  CONSTRAINT `fk_Ciudad_Provincia1`
    FOREIGN KEY (`Provincia_idProvincia`)
    REFERENCES `mydb`.`Provincia` (`idProvincia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Ciudad_Provincia1_idx` ON `mydb`.`Ciudad` (`Provincia_idProvincia` ASC);


-- -----------------------------------------------------
-- Table `mydb`.`TipoPropiedad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`TipoPropiedad` (
  `idTipoPropiedad` INT NOT NULL,
  `nombreTipoProp` VARCHAR(45) NULL,
  PRIMARY KEY (`idTipoPropiedad`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Avisos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Avisos` (
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`PlanesCobro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`PlanesCobro` (
  `idPlanesCobro` INT NOT NULL,
  `precioPlan` DOUBLE NULL,
  `tipoPlan` VARCHAR(45) NULL,
  `numDias` INT NULL,
  PRIMARY KEY (`idPlanesCobro`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Publicacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Publicacion` (
  `idPublicacion` INT NOT NULL AUTO_INCREMENT,
  `precio` DOUBLE NULL,
  `numPisos` INT NULL,
  `numBanos` INT NULL,
  `superficieConstr` INT NULL,
  `superficieAbierta` INT NULL,
  `direccion` VARCHAR(45) NULL,
  `numHabitaciones` INT NULL,
  `antiguedad` INT NULL,
  `descripcionPublic` VARCHAR(10000) NULL,
  `tituloPublic` VARCHAR(45) NULL,
  `PlanesCobro_idPlanesCobro` INT NOT NULL,
  `TipoPropiedad_idTipoPropiedad` INT NOT NULL,
  `Ciudad_idCiudad` INT NOT NULL,
  `Usuario_idUsuario` INT NOT NULL,
  PRIMARY KEY (`idPublicacion`),
  CONSTRAINT `fk_Publicacion_PlanesCobro1`
    FOREIGN KEY (`PlanesCobro_idPlanesCobro`)
    REFERENCES `mydb`.`PlanesCobro` (`idPlanesCobro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Publicacion_TipoPropiedad1`
    FOREIGN KEY (`TipoPropiedad_idTipoPropiedad`)
    REFERENCES `mydb`.`TipoPropiedad` (`idTipoPropiedad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Publicacion_Ciudad1`
    FOREIGN KEY (`Ciudad_idCiudad`)
    REFERENCES `mydb`.`Ciudad` (`idCiudad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Publicacion_Usuario1`
    FOREIGN KEY (`Usuario_idUsuario`)
    REFERENCES `mydb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Publicacion_PlanesCobro1_idx` ON `mydb`.`Publicacion` (`PlanesCobro_idPlanesCobro` ASC);

CREATE INDEX `fk_Publicacion_TipoPropiedad1_idx` ON `mydb`.`Publicacion` (`TipoPropiedad_idTipoPropiedad` ASC);

CREATE INDEX `fk_Publicacion_Ciudad1_idx` ON `mydb`.`Publicacion` (`Ciudad_idCiudad` ASC);

CREATE INDEX `fk_Publicacion_Usuario1_idx` ON `mydb`.`Publicacion` (`Usuario_idUsuario` ASC);


-- -----------------------------------------------------
-- Table `mydb`.`InfoContacto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`InfoContacto` (
  `idInfoContacto` INT NOT NULL,
  `emailCont` VARCHAR(45) NULL,
  `telefonoCont` VARCHAR(45) NULL,
  `fijoCont` VARCHAR(45) NULL,
  `Usuario_idUsuario` INT NOT NULL,
  PRIMARY KEY (`idInfoContacto`),
  CONSTRAINT `fk_InfoContacto_Usuario1`
    FOREIGN KEY (`Usuario_idUsuario`)
    REFERENCES `mydb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_InfoContacto_Usuario1_idx` ON `mydb`.`InfoContacto` (`Usuario_idUsuario` ASC);


-- -----------------------------------------------------
-- Table `mydb`.`NumHabitaciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`NumHabitaciones` (
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Caracteristicas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Caracteristicas` (
  `idCaracteristicas` INT NOT NULL,
  `descriCaract` VARCHAR(150) NULL,
  `tipoCaract` VARCHAR(45) NULL,
  PRIMARY KEY (`idCaracteristicas`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Consultas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Consultas` (
  `idConsultas` INT NOT NULL,
  `fechaCons` DATETIME NULL,
  `Publicacion_idPublicacion` INT NOT NULL,
  `InfoContacto_idInfoContacto` INT NOT NULL,
  PRIMARY KEY (`idConsultas`),
  CONSTRAINT `fk_Consultas_Publicacion1`
    FOREIGN KEY (`Publicacion_idPublicacion`)
    REFERENCES `mydb`.`Publicacion` (`idPublicacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Consultas_InfoContacto1`
    FOREIGN KEY (`InfoContacto_idInfoContacto`)
    REFERENCES `mydb`.`InfoContacto` (`idInfoContacto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Consultas_Publicacion1_idx` ON `mydb`.`Consultas` (`Publicacion_idPublicacion` ASC);

CREATE INDEX `fk_Consultas_InfoContacto1_idx` ON `mydb`.`Consultas` (`InfoContacto_idInfoContacto` ASC);


-- -----------------------------------------------------
-- Table `mydb`.`Fotos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Fotos` (
  `idFotos` INT NOT NULL,
  `urlImg` VARCHAR(45) NULL,
  `Publicacion_idPublicacion` INT NOT NULL,
  PRIMARY KEY (`idFotos`),
  CONSTRAINT `fk_Fotos_Publicacion1`
    FOREIGN KEY (`Publicacion_idPublicacion`)
    REFERENCES `mydb`.`Publicacion` (`idPublicacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Fotos_Publicacion1_idx` ON `mydb`.`Fotos` (`Publicacion_idPublicacion` ASC);


-- -----------------------------------------------------
-- Table `mydb`.`Publicacion_has_Caracteristicas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Publicacion_has_Caracteristicas` (
  `Publicacion_idPublicacion` INT NOT NULL,
  `Caracteristicas_idCaracteristicas` INT NOT NULL,
  PRIMARY KEY (`Publicacion_idPublicacion`, `Caracteristicas_idCaracteristicas`),
  CONSTRAINT `fk_Publicacion_has_Caracteristicas_Publicacion1`
    FOREIGN KEY (`Publicacion_idPublicacion`)
    REFERENCES `mydb`.`Publicacion` (`idPublicacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Publicacion_has_Caracteristicas_Caracteristicas1`
    FOREIGN KEY (`Caracteristicas_idCaracteristicas`)
    REFERENCES `mydb`.`Caracteristicas` (`idCaracteristicas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Publicacion_has_Caracteristicas_Caracteristicas1_idx` ON `mydb`.`Publicacion_has_Caracteristicas` (`Caracteristicas_idCaracteristicas` ASC);

CREATE INDEX `fk_Publicacion_has_Caracteristicas_Publicacion1_idx` ON `mydb`.`Publicacion_has_Caracteristicas` (`Publicacion_idPublicacion` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
